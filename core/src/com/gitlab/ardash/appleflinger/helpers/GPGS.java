/*******************************************************************************
 * Copyright (C) 2015-2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.gitlab.ardash.appleflinger.helpers;

public class GPGS {
    public static final String ACH_TRIPPLE_POP = "CgkIrZj_yLcCEAIQAg"; 
    public static final String ACH_FOURFOLD_POP = "CgkIrZj_yLcCEAIQAw"; 
    public static final String ACH_FIVEFOLD_POP = "CgkIrZj_yLcCEAIQBA"; 
    public static final String ACH_WREAK_HAVOC = "CgkIrZj_yLcCEAIQBQ"; 
    public static final String ACH_BEGINNERS_LUCK = "CgkIrZj_yLcCEAIQBg"; 
    public static final String ACH_POINTS_FARMER = "CgkIrZj_yLcCEAIQBw"; 
    public static final String ACH_BEGINNER_STREAK = "CgkIrZj_yLcCEAIQCA"; 
    public static final String LEAD_MOST_POINTS = "CgkIrZj_yLcCEAIQCQ"; 

}
